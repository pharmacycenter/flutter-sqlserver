import 'package:flutter/material.dart';
import 'package:login/providers/usuario_provider.dart';
import 'package:login/screens/home_screen.dart';
import 'package:login/screens/login.dart';
import 'package:login/screens/registro.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider(create: (_) => Usuario_provider())],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'MaterialApp',
          routes: {
            '/': (context) => const LoginScreen(),
            '/home': (context) => const HomeScreen(),
            '/registro': (context) => const RegistroScreen(),
          },
          initialRoute: 'login'),
    );
  }
}
