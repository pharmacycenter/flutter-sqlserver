import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:login/constants.dart';
import 'package:login/models/usuario.dart';

const urlapi = url;

// ignore: camel_case_types
class Usuario_provider with ChangeNotifier {
  List<Usuario> usuarios = [];

  Usuario_provider() {
    getUsuarios();
  }

  getUsuarios() async {
    final url1 = Uri.http(urlapi, "api/Usuario");
    final resp = await http.get(url1, headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": "true",
      'Content-type': 'application/json',
      'Accept': 'application/json'
    });
    final response = usuarioFromJson(resp.body);
    usuarios = response;
    notifyListeners();
  }

  Future<bool> registrarUsuario(String email, String password) async {
    final url1 = Uri.http(urlapi, "api/Usuario");
    final nuevoUsuario = Usuario(
      id: usuarios.length + 1,
      email: email,
      password: password,
    );

    final resp = await http.post(
      url1,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": "true",
        'Content-type': 'application/json',
        'Accept': 'application/json'
      },
      body: usuarioToJson([nuevoUsuario]),
    );

    if (resp.statusCode == 201) {
      usuarios.add(nuevoUsuario);
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }
}
