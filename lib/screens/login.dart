import 'package:flutter/material.dart';
import 'package:login/providers/usuario_provider.dart';
import 'package:login/widgets/input_decoration.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          children: [
            cajapurple(size),
            cajapersona(),
            loginform(context),
          ],
        ),
      ),
    );
  }

  SingleChildScrollView loginform(BuildContext context) {
    final usuarioProvider = Provider.of<Usuario_provider>(context);
    var txtCorreo = TextEditingController();
    var txtPassword = TextEditingController();
    return SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(height: 250),
          Container(
            padding: const EdgeInsets.all(20),
            margin: const EdgeInsets.symmetric(horizontal: 30),
            width: double.infinity,
            // height: 375,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(25),
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: 15,
                    offset: Offset(0, 5),
                  )
                ]),
            child: Column(children: [
              const SizedBox(
                height: 10,
              ),
              Text('Login', style: Theme.of(context).textTheme.headlineSmall),
              const SizedBox(
                height: 30,
              ),
              Form(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  child: Column(
                    children: [
                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        autocorrect: false,
                        controller: txtCorreo,
                        decoration: InputDecorations.inputDecoration(
                            hintText: "ejemplo@gmail.com",
                            labelText: "Correo electronico",
                            icono: const Icon(Icons.alternate_email_rounded)),
                        validator: (value) {
                          String pattern =
                              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                          RegExp regExp = RegExp(pattern);
                          return regExp.hasMatch(value ?? '')
                              ? null
                              : 'El valor ingresado no es un correo';
                        },
                      ),
                      const SizedBox(height: 30),
                      TextFormField(
                        autocorrect: false,
                        obscureText: true,
                        controller: txtPassword,
                        decoration: InputDecorations.inputDecoration(
                            hintText: '******',
                            labelText: 'Password',
                            icono: const Icon(Icons.lock_outline)),
                        validator: (value) {
                          return (value != null && value.length >= 6)
                              ? null
                              : 'El valor ingresado debe ser mayor a 6 digitos';
                        },
                      ),
                      const SizedBox(height: 30),
                      MaterialButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          disabledColor: Colors.grey,
                          color: Colors.deepPurple,
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 80, vertical: 15),
                            child: const Text(
                              'Ingresar',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          onPressed: () {
                            var usuarios = usuarioProvider.usuarios;
                            if (usuarios
                                    .any((e) => e.email == txtCorreo.text) &&
                                usuarios.any(
                                    (e) => e.password == txtPassword.text)) {
                              Navigator.pushReplacementNamed(context, '/home');
                            } else {
                              print('buuuu');
                            }
                          })
                    ],
                  ))
            ]),
          ),
          const SizedBox(
            height: 50,
          ),
          InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/registro');
            },
            onLongPress: () {},
            child: const Text('No tienes cuenta? Registrate'),
          ),
        ],
      ),
    );
  }

  SafeArea cajapersona() {
    return SafeArea(
      child: Container(
        margin: const EdgeInsets.only(top: 30),
        width: double.infinity,
        child: const Icon(
          Icons.person_pin,
          color: Colors.white,
          size: 100,
        ),
      ),
    );
  }

  Container cajapurple(Size size) {
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(colors: [
        Color.fromRGBO(63, 63, 156, 1),
        Color.fromRGBO(90, 70, 178, 1),
      ])),
      width: double.infinity,
      height: size.height * 0.4,
      child: Stack(children: [
        Positioned(top: 90, left: 30, child: burbuja()),
        Positioned(top: -40, left: -30, child: burbuja()),
        Positioned(top: -50, right: -20, child: burbuja()),
        Positioned(bottom: -50, left: 10, child: burbuja()),
        Positioned(bottom: 120, right: -20, child: burbuja()),
      ]),
    );
  }

  Container burbuja() {
    return Container(
      width: 100,
      height: 100,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          color: const Color.fromRGBO(255, 255, 255, 0.05)),
    );
  }
}
